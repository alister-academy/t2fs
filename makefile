#
# Makefile ESQUELETO
#
# DEVE ter uma regra "all" para geração da biblioteca
# regra "clean" para remover todos os objetos gerados.
#
# NECESSARIO adaptar este esqueleto de makefile para suas necessidades.
#
# 

CC=gcc
LIB_DIR=./lib/
INC_DIR=./include/
BIN_DIR=./bin/
SRC_DIR=./src/
TESTS_BIN_DIR=./teste/bin/
TESTS_SRC_DIR=./teste/src/

all: t2fs superblock fat record main lib-t2fs tests main-binary

main-binary:
	$(CC) -o $(BIN_DIR)main $(BIN_DIR)main.o $(LIB_DIR)libt2fs.a -Wall -m32

main:
	$(CC) -c $(SRC_DIR)main.c -I$(INC_DIR) -Wall -m32
	mv main.o $(BIN_DIR)

t2fs:
	$(CC) -c $(SRC_DIR)t2fs.c -I$(INC_DIR) -Wall -m32
	mv t2fs.o $(BIN_DIR)

lib-t2fs:
	ar crs $(LIB_DIR)libt2fs.a $(BIN_DIR)t2fs.o $(BIN_DIR)superblock.o $(BIN_DIR)fat.o $(BIN_DIR)record.o $(LIB_DIR)apidisk.o

# Development #
superblock:
	$(CC) -c $(SRC_DIR)superblock.c -I$(INC_DIR) -Wall -m32
	mv superblock.o $(BIN_DIR)

fat:
	$(CC) -c $(SRC_DIR)fat.c -I$(INC_DIR) -Wall -m32
	mv fat.o $(BIN_DIR)

record:
	$(CC) -c $(SRC_DIR)record.c -I$(INC_DIR) -Wall -m32
	mv record.o $(BIN_DIR)


## Tests ##
tests: identify cwd dirinfo openclose

identify:
	$(CC) -o $(TESTS_BIN_DIR)identify $(TESTS_SRC_DIR)identify.c $(LIB_DIR)libt2fs.a -I$(INC_DIR) -Wall -m32

cwd:
	$(CC) -o $(TESTS_BIN_DIR)cwd $(TESTS_SRC_DIR)cwd.c $(LIB_DIR)libt2fs.a -I$(INC_DIR) -Wall -m32

dirinfo:
	$(CC) -o $(TESTS_BIN_DIR)dirinfo $(TESTS_SRC_DIR)dirinfo.c $(LIB_DIR)libt2fs.a -I$(INC_DIR) -Wall -m32

openclose:
	$(CC) -o $(TESTS_BIN_DIR)openclose $(TESTS_SRC_DIR)openclose.c $(LIB_DIR)libt2fs.a -I$(INC_DIR) -Wall -m32


## Clean ##
clean: clean-bin clean-lib clean-obj clean-tests

clean-bin:
	rm -rf $(BIN_DIR)*

clean-folder:
	rm -rf *~

clean-obj:
	rm -rf $(BIN_DIR)*.o

clean-src:
	rm -rf $(SRC_DIR)*~

clean-lib:
	rm -rf $(LIB_DIR)*.a

clean-include:
	rm -rf $(INC_DIR)*~

clean-tests:
	rm -rf $(TESTS_BIN_DIR)*