#ifndef __RECORD_
#define __RECORD_

#include "../include/t2fs.h" /* estrutura dos registros */
#include "../include/superblock.h" /* estrutura dos superblock contendo 
informacao do setor de inicio do root */



/* Implementacao de metodos de leitura do disco e 
*  manipulacao de arvore de diretorios
*/

// cwd = current working directory

// Lista de records
typedef struct t2fs_record_list
{
	struct t2fs_record_node* first;
	int size;
} t2fs_record_list;

// Nodos da arvore de diretorios
typedef struct t2fs_record_node
{
	struct t2fs_record record;
	struct t2fs_record_node* parent;

	t2fs_record_list* sub_folders;
	t2fs_record_list* files;

	struct t2fs_record_node* next;
} t2fs_record_node;



// Arvore contendo as informacoes de subdiretorios e arquivos...
t2fs_record_node root;
t2fs_record_node* cwd;
t2fs_record_node fileHandlers[20];
t2fs_record_node dirHandlers[20];


/* Inicializa arvore ( APENAS O ROOT NO MOMENTO ) 
*  a partir do setor inicial do root
*  Retorna 0 caso a arvore seja inicializada com sucesso,
* -1 caso contrario
*/
int initRecordTree();


/* Inicializa subpastas de um nodo, recursivamente 
*  Retorna 0 caso a arvore seja inicializada com sucesso,
* -1 caso contrario
*/
int initSubFolders(struct t2fs_record_node* parent);




/* Inicializa os registros (sub pastas e arquivos) de um nodo
*  atraves das informacoes contidas no disco
*  Retorna 0 caso o nodo seja inicializada com sucesso,
* -1 caso contrario
*/
int initNodeRecordsFromDiskSector(t2fs_record_node* node, int firstSector, int lastSector);

// Adiciona um subdiretorio a lista de subdiretorios do diretorio pai
int addSubDir(struct t2fs_record_node* parent, struct t2fs_record_node* subDir);

// Remove um subdiretorio a lista de subdiretorios do diretorio pai
int removeSubDir(struct t2fs_record_node* parent, struct t2fs_record_node* subDir);



// Adiciona um arquivo a lista de arquivos do diretorio pai
int addFile(struct t2fs_record_node* parent, struct t2fs_record_node* file);

// Remove um arquivo a lista de arquivos do diretorio pai
int removeFile(struct t2fs_record_node* parent, struct t2fs_record_node* file);



// Limpa a lista de subdiretorios
int clearSubDirs();

// Limpa a lista de arquivos
int clearFiles();


// Imprime o conteudo de um nodo (pasta ou arquivo)
void printRecord(struct t2fs_record_node* node);

// Imprime as sub pastas do diretorio pai
void printSubDirs(struct t2fs_record_node* parent);

// Imprime os arquivos do diretorio pai
void printFiles(struct t2fs_record_node* parent);

// Inicializa o conteudo de um nodo atraves de um cluster inicial
int initNodeRecordsFromDiskCluster(struct t2fs_record_node* node, int firstCluster, t2fs_record_node* parent);


int cd(char* folder);

int cdUp();
#endif