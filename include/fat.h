#ifndef __FAT_
#define __FAT_

/* ESTRUTURAS DA FAT
*  CADA ELEMENTO REPRESENTA UM CLUSTER DE DADOS
*
*  CUIDADO:
*      NUMEROS 0 E 1 SAO RESERVADOS!
*      O PRIMEIRO ELEMENTO DA LISTA CORRESPONDE 
*      AO PRIMEIRO CLUSTER DE DADOS, E RECEBE O NUMERO 2!
*/

/* Valores 'especiais' na FAT */
#define TYPEVAL_LIVRE    0x0000
#define TYPEVAL_NAOPODEAPARECER 0x0001
#define TYPEVAL_BADSECTOR     0x0FFE
#define TYPEVAL_ULTIMOCLUSTER   0xFFFF

char* pFAT;
char* sFAT;

/* Inicializacao dos dados da FAT primaria
*  Parametros:
*      size: numero de clusters / elementos da FAT
*
*  Saida:
*      0 em caso de sucesso, -1 caso contrario
*/
int initPrimaryFAT(int size);


/* Inicializacao dos dados da FAT secundaria
*      size: numero de clusters / elementos da FAT
*
*  Saida:
*      0 em caso de sucesso, -1 caso contrario
*/
int initSecondaryFAT(int size, int sector);


/* Imprime o conteudo de uma FAT
*  Parametros:
*      fat: buffer com o conteudo da FAT
*      size: numero de clusters / elementos listados na FAT
*/
void printFATData(char* fat, int size);


/* Calcula qual o setor referente ao cluster
*  passado como argumento, atraves da leitura da FAT
*  Parametros:
*      cluster: valor do cluster
*
*  Saída:
*      Setor correspondente ao cluster
*/
int getSectorFromCluster(unsigned short int cluster);


/* Retorna o proximo cluster do arquivo de
*  acordo com o conteudo da FAT
*  Parametros:
*      cluster: 'valor' do cluster (cluster 2, 3, ...)
*
*  Saída:
*      Proximo cluster
*      Retorna -1 quando ocorre algum ERRO
*/
unsigned short int getNextCluster(int cluster);


/* Retorna o valor do cluster
*      cluster: 'valor' do cluster (cluster 2, 3, ...)
*  Saída:
*      valor do cluster (livre, bad sector, ...)
*/
unsigned short int getCluster(int cluster);

#endif //__FAT
