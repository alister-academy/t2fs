#ifndef __SUPERBLOCK_
#define __SUPERBLOCK_

#include "../include/t2fs.h" /* estrutura do superbloco */

/* Superbloco */
struct t2fs_superbloco _superblock;

unsigned int numClusters;

int initSuperblock();
void printSuperblockData();

#endif //__SUPERBLOCK_
