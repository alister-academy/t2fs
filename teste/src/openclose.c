/*
* dirinfo: 
* Faz chamadas de opendir e readdir para obter a 
* informação do conteúdo das pastas abertas.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/t2fs.h"

int main()
{

	char* cd = (char*) malloc(sizeof(char)*MAX_FILE_NAME_SIZE);
	if(cd == NULL) return -1;
	
	// abre a pasta root '/'
	strcpy(cd, ".");

	DIR2 handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);

	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);

	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);

	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);

	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);

	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);


	printf("\n");
	if(closedir2 (10) == 0) printf("closedir2 ok \t DIR2: 10\n");
	if(closedir2 (0) == 0) printf("closedir2 ok \t DIR2: 0\n");
	if(closedir2 (19) == 0) printf("closedir2 ok \t DIR2: 19\n");
	printf("\n");

	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);
	handle = opendir2 (cd);
	if(handle != -1) printf("opendir2 ok \t DIR2: %i \n",handle);

	free(cd);
	return 0;
}