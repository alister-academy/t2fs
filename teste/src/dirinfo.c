/*
* dirinfo: 
* Faz chamadas de opendir e readdir para obter a 
* informação do conteúdo das pastas abertas.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/t2fs.h"

int main()
{

	char* cd = (char*) malloc(sizeof(char)*MAX_FILE_NAME_SIZE);
	if(cd == NULL) return -1;
	
	// abre a pasta root '/'
	strcpy(cd, ".");

	DIR2 status1 = opendir2 (cd);
	if(status1 != -1) printf("opendir2 ok - DIR2: %i \n",status1);

	// fica iterando sobre os registros do root acessado...
	DIRENT2 entry;
	if(readdir2(status1, &entry) == 0) printf("readdir2 ok \t entry: %s \n", entry.name);
	if(readdir2(status1, &entry) == 0) printf("readdir2 ok \t entry: %s \n", entry.name);
	if(readdir2(status1, &entry) == 0) printf("readdir2 ok \t entry: %s \n", entry.name);	
	if(readdir2(status1, &entry) == 0) printf("readdir2 ok \t entry: %s \n", entry.name);	
	if(readdir2(status1, &entry) == 0) printf("readdir2 ok \t entry: %s \n", entry.name);	
	if(readdir2(status1, &entry) == 0) printf("readdir2 ok \t entry: %s \n", entry.name);	

	free(cd);
	return 0;
}