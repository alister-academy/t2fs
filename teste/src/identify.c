/*
* identify: 
* O teste foi criado para testar a funcionalidade de identificação 
* do desenvolvedor. Para isso, é feita a chamada da função com 
* argumentos corretos e depois, com argumentos incorretos.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/t2fs.h"

int main()
{
	char* developer = (char*) malloc(sizeof(char)*50);

	// identify com parametros ok
	if(identify2(developer, 50) == 0) printf("a) developer: %s \n", developer);

	// identify com size menor que o esperado
	if(identify2(developer, 10) == 0) printf("b) developer: %s \n", developer);

	developer = (char*) realloc(developer, 10);
	if(developer == NULL) return -1;
	
	// identify com char* com alocacao menor que a esperada:
	// faz com que ocorra um crash na chamada do free()
	// ja que o strcpy() SOBRESCREVE UM ESPACO ALEM DO ALOCADO
	// assim, ao chamar o free(), o programa tenta apagar mais
	// do que deve, causando o invalid access
	// if(identify2(developer, 50) == 0) printf("c) developer: %s \n", developer);
	
	// identify com char* com alocacao e size menores que os esperados
	if(identify2(developer, 10) == 0) printf("d) developer: %s \n", developer);
	
	printf("\n");


	free(developer);
	return 0;
}