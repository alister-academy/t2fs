/*
* cwd: 
* O código em questão realiza a mudança de diretório atual 
* dentro da árvore de registros implementada e realiza 
* chamadas da getcwd2 para obter o filepath atual.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/t2fs.h"

#define FILEPATH_MAX_SIZE 512

int main()
{

	char* cwd = (char*) malloc(sizeof(char)*FILEPATH_MAX_SIZE);
	if(cwd == NULL) return -1;

	// indica que o diretorio atual eh o root '/'
	if(getcwd2(cwd, FILEPATH_MAX_SIZE) == 0) printf("current working directory: %s \n", cwd);
	else printf("failed to get current directory \n");


	char* cd = (char*) malloc(sizeof(char)*MAX_FILE_NAME_SIZE);
	if(cd == NULL) {free(cwd); return -1;}

	// abre a pasta root '/'
	strcpy(cd , ".");
	DIR2 status1 = opendir2 (cd);
	if(status1 != -1) printf("opendir2 ok - DIR2 handle: %i \n",status1);
	// pega o primeiro registro do root que foi acessado...
	// se for pasta, ele usa pra dar cd nela...
	DIRENT2 entry;
	
	if(readdir2(status1, &entry) == 0)
	{
		printf("readdir2 ok \t entry: %s \n", entry.name);
		// tenta acessar a primeira subpasta, se existir uma
		if(entry.fileType == 1)
		{
			strcpy(cd, entry.name);
			printf("ch [%s] ... ", cd);
			if(chdir2(cd) == 0) printf("ok \n");
			else printf("failed \n");
			// mostra o caminho atual
			if(getcwd2(cwd, FILEPATH_MAX_SIZE) == 0) printf("current working directory: %s \n\n", cwd);
			else printf("failed to get current directory \n");
		}
	}
	// tenta acessar uma pasta nao existente
	strcpy(cd, "some_random_folder");
	printf("ch [%s] ... ", cd);
	if(chdir2(cd) == 0) printf("ok \n");
	else printf("failed \n");
	// mostra o caminho atual
	if(getcwd2(cwd, FILEPATH_MAX_SIZE) == 0) printf("current working directory: %s \n\n", cwd);
	else printf("failed to get current directory \n");

	// tenta acessar novamente a pasta atual
	strcpy(cd, ".");
	printf("ch [%s] ... ", cd);
	if(chdir2(cd) == 0) printf("ok \n");
	else printf("failed \n");
	// mostra o caminho atual
	if(getcwd2(cwd, FILEPATH_MAX_SIZE) == 0) printf("current working directory: %s \n\n", cwd);
	else printf("failed to get current directory \n");

	// tenta acessar novamente a pasta pai
	strcpy(cd, "..");
	printf("ch [%s] ... ", cd);
	if(chdir2(cd) == 0) printf("ok \n");
	else printf("failed \n");
	// mostra o caminho atual
	if(getcwd2(cwd, FILEPATH_MAX_SIZE) == 0) printf("current working directory: %s \n", cwd);
	else printf("failed to get current directory \n");

	free(cwd);
	free(cd);
	return 0;
}