#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/t2fs.h" /* estrutura do superbloco */
#include "../include/fat.h" /* prototipos */
#include "../include/superblock.h" /* superbloco para a leitura do sectorsPerCluster */
#include "../include/apidisk.h" /* metodos para leitura do setor 0 (dados do superbloco) */

#define SUCCESS 0
#define ERROR -1

int initPrimaryFAT(int size)
{
	pFAT = (char*) malloc(sizeof(WORD)*size);
	if(read_sector(1, pFAT) != SUCCESS)
	{
		// printf("Leitura de FAT mal sucedida. \n");
		return ERROR;
	}

	// printf("Imprimindo conteudo da FAT primaria...\n");
	// printFATData(pFAT, size);
	return SUCCESS;
}

int initSecondaryFAT(int size, int sector)
{
	sFAT = (char*) malloc(sizeof(WORD)*size);
	if(read_sector(sector, sFAT) != SUCCESS)
	{
		// printf("Leitura de FAT mal sucedida. \n");
		return ERROR;
	}
	// printf("Imprimindo conteudo da FAT secundaria...\n");
	// printFATData(sFAT, size);
	return SUCCESS;	

}

void printFATData(char* fat, int size)
{
	int i;
	for(i = 0; i < size; i++)
	{
		printf("%.4x ", (WORD) fat[i]);	
	}	
	printf("\n\n");
}

int getSectorFromCluster(unsigned short int cluster)
{
	int sectorsPerCluster = _superblock.SectorPerCluster;
	int firstDataSector = _superblock.DataSectorStart;

	// lets say we want the first cluster == first sector
	// cluster = 2 should return the first data sector
	// thus, "cluster - 2"
	int sector = firstDataSector + (sectorsPerCluster * (cluster - 2));

	return sector;
}

unsigned short int getNextCluster(int cluster)
{
	// clusters 0 e 1 reservados
	// FAT[0] e na verdade cluster 2
	int nextCluster = pFAT[cluster-2];

	if(nextCluster == TYPEVAL_LIVRE ||
		nextCluster == TYPEVAL_BADSECTOR ||
		nextCluster == TYPEVAL_NAOPODEAPARECER) return ERROR;


	return nextCluster;
}

unsigned short int getCluster(int cluster)
{
	return pFAT[cluster-2];
}