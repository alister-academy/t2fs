#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/record.h" /* estrutura de lista, nodo, arvore e prototipos */
#include "../include/t2fs.h" /* estrutura dos registros */
#include "../include/fat.h" /* metodos para pegar o setor atraves de um cluster */
#include "../include/apidisk.h" /* metodos para leitura dos setores do root */


#define SUCCESS 0
#define ERROR -1

#define MAX_RECORDS_IN_A_SECTOR (SECTOR_SIZE / sizeof(struct t2fs_record))

// Inicializa arvore a partir do setor do root
int initRecordTree()
{
	// Inicializacao do diretorio raiz
	root.record.TypeVal = TYPEVAL_DIRETORIO;
	strncpy(root.record.name, "\\", sizeof(root.record.name));
	root.record.bytesFileSize = 0;
	root.record.firstCluster = 0;

	root.parent = NULL;
	root.sub_folders = (t2fs_record_list*) malloc(sizeof(t2fs_record_list));
	if(root.sub_folders == NULL) return ERROR;
	root.sub_folders->size = 0;
	root.files = (t2fs_record_list*) malloc(sizeof(t2fs_record_list));
	if(root.files == NULL) return ERROR;
	root.files->size = 0;

	unsigned int rootSectorStart = _superblock.RootSectorStart;
	unsigned int dataSectorStart = _superblock.DataSectorStart;
	initNodeRecordsFromDiskSector(&root, rootSectorStart, dataSectorStart-1);

	cwd = &root;

	initSubFolders(&root);

	return SUCCESS;
}


int initSubFolders(struct t2fs_record_node* parent)
{
	// se entrar um pai nulo, algo esta errado...
	if(parent == NULL) return ERROR;

	// caso nao tenham subpastas para serem inicializadas...
	if(parent->sub_folders == NULL) return SUCCESS;

	struct t2fs_record_node* currentNode = parent->sub_folders->first;

	int i;
	for(i = 0; i < parent->sub_folders->size; i++)
	{
		// nao chama a funcao para o diretorio atual e o diretorio pai
		// se nao, teria loop eterno
		if(strcmp(currentNode->record.name, "..") == 0 || strcmp(currentNode->record.name, ".") == 0) 
			continue;

		// printRecord(currentNode);

		initNodeRecordsFromDiskCluster(currentNode, currentNode->record.firstCluster, parent);
		initSubFolders(currentNode);
		currentNode = currentNode->next;
	}


	return SUCCESS;
}


// Adiciona um subdiretorio a lista de subdiretorios do diretorio pai
int addSubDir(struct t2fs_record_node* parent, struct t2fs_record_node* subDir)
{
	// printf("Adicionando sub pasta...\n");
	// printRecord(subDir);

	// Diretorio pai nao foi alocado
	if(parent == NULL) return ERROR;

	// Lista de sub pastas do Diretorio pai nao foi alocada
	if(parent->sub_folders == NULL) return ERROR;

	// Lista de sub pastas vazia
	if(parent->sub_folders->size == 0)
	{
		parent->sub_folders->first = subDir;
		parent->sub_folders->size++;
		return SUCCESS;
	}

	if(parent->sub_folders->size > 0)
	{
		int i;
		struct t2fs_record_node* currentSubFolder = parent->sub_folders->first;
		for(i = 1; i < parent->sub_folders->size; i++)
		{
			currentSubFolder = currentSubFolder->next;
		}
		// Adiciona a sub pasta ao final da lista de sub pastas do
		// diretorio pai
		currentSubFolder->next = subDir;
		parent->sub_folders->size++;
		return SUCCESS;
	}
	return ERROR;

	//TO-DO
}

// Remove um subdiretorio a lista de subdiretorios do diretorio pai
int removeSubDir(struct t2fs_record_node* parent, struct t2fs_record_node* subDir)
{
	// printf("Removendo sub pasta...\n");
	// printRecord(subDir);

	// Diretorio pai nao foi alocado
	if(parent == NULL) return ERROR;

	// Lista de sub pastas do Diretorio pai nao foi alocada
	if(parent->sub_folders == NULL) return ERROR;

	// Lista de sub pastas vazia
	if(parent->sub_folders->size == 0)	return SUCCESS;

	if(parent->sub_folders->size > 0)
	{
		struct t2fs_record_node* currentSubFolder = parent->sub_folders->first;

		// Lista de sub pastas tem apenas um elemento
		if(currentSubFolder == subDir)
		{
			parent->sub_folders->first = NULL;
			parent->sub_folders->size--;
			free(subDir->sub_folders);
			subDir->sub_folders = NULL;
			free(subDir->files);
			subDir->files = NULL;
			free(subDir); // free pedindo pra dar errado!
			subDir = NULL;
			return SUCCESS;
		}

		// caso possua mais, itera sobre as pastas
		int i;
		struct t2fs_record_node* previousSubFolder = NULL;
		for(i = 1; i < parent->sub_folders->size; i++)
		{
			if(currentSubFolder == subDir)
			{
				previousSubFolder->next = subDir->next;
				parent->sub_folders->size--;
				free(subDir->sub_folders);
				subDir->sub_folders = NULL;
				free(subDir->files);
				subDir->files = NULL;
				free(subDir); // free pedindo pra dar errado!
				subDir = NULL;
				return SUCCESS;
			}
			else 
			{
				previousSubFolder = currentSubFolder;
				currentSubFolder = currentSubFolder->next;
			}
		}
	}
	// Se a sub pasta nao esta na lista...
	return SUCCESS;
}



// Adiciona um arquivo a lista de arquivos do diretorio pai
int addFile(struct t2fs_record_node* parent, struct t2fs_record_node* file)
{
	// printf("Adicionando arquivo...\n");
	// printRecord(file);
	
	// Diretorio pai nao foi alocado
	if(parent == NULL) return ERROR;

	// Lista de arquivos do Diretorio pai nao foi alocada
	if(parent->files == NULL) return ERROR;

	// Lista de arquivos vazia
	if(parent->files->size == 0)
	{
		parent->files->first = file;
		parent->files->size++;
		return SUCCESS;
	}

	if(parent->files->size > 0)
	{
		int i;
		struct t2fs_record_node* currentFile = parent->files->first;
		for(i = 1; i < parent->files->size; i++)
		{
			currentFile = currentFile->next;
		}
		// Adiciona o arquivo ao final da lista de arquivos do
		// diretorio pai
		currentFile->next = file;
		parent->files->size++;
		return SUCCESS;
	}
	return SUCCESS;
}

// Remove um arquivo a lista de arquivos do diretorio atual
int removeFile(struct t2fs_record_node* parent, struct t2fs_record_node* file)
{
	// printf("Removendo arquivo...\n");
	// printRecord(file);

	// Diretorio pai nao foi alocado
	if(parent == NULL) return ERROR;

	// Lista de arquivos do Diretorio pai nao foi alocada
	if(parent->files == NULL) return ERROR;

	// Lista de arquivos vazia
	if(parent->files->size == 0)	return SUCCESS;

	if(parent->files->size > 0)
	{
		struct t2fs_record_node* currentFile = parent->files->first;

		// Lista de arquivos tem apenas um elemento
		if(currentFile == file)
		{
			parent->files->first = NULL;
			parent->files->size--;
			free(file->files);
			file->files = NULL;
			free(file->files);
			file->files = NULL;
			free(file); // free pedindo pra dar errado!
			file = NULL;
			return SUCCESS;
		}

		// caso possua mais, itera sobre os arquivos
		int i;
		struct t2fs_record_node* previousFile = NULL;
		for(i = 1; i < parent->files->size; i++)
		{
			if(currentFile == file)
			{
				previousFile->next = file->next;
				parent->files->size--;
				free(file->files);
				file->files = NULL;
				free(file->files);
				file->files = NULL;
				free(file); // free pedindo pra dar errado!
				file = NULL;
				return SUCCESS;
			}
			else 
			{
				previousFile = currentFile;
				currentFile = currentFile->next;
			}
		}
	}
	// Se o arquivo nao esta na lista...
	return SUCCESS;
}



// Limpa a lista de subdiretorios
int clearSubDirs()
{
	//TO-DO
	return ERROR;
}

// Limpa a lista de arquivos
int clearFiles()
{
	//TO-DO
	return ERROR;
}

void printRecord(struct t2fs_record_node* node)
{
	if(node == NULL) printf("NULL node!\n");
	if(node)
	{
		struct t2fs_record record = node->record;
		printf("\n");
		printf("* Name: %s\n", record.name);
		printf("* Size: %i\n", record.bytesFileSize);
		printf("* First cluster: %u\n", record.firstCluster);
	}
}


void printSubDirs(struct t2fs_record_node* parent)
{
	// Diretorio pai nao foi alocado
	if(parent != NULL)
	{
		// Lista de sub pastas do Diretorio pai nao foi alocada
		if(parent->sub_folders != NULL)
		{
			int i;
			struct t2fs_record_node* currentSubFolder = parent->sub_folders->first;
			for(i = 0; i < parent->sub_folders->size; i++)
			{
				printRecord(currentSubFolder);
				currentSubFolder = currentSubFolder->next;
			}
		}
	}
}


void printFiles(struct t2fs_record_node* parent)
{
	// Diretorio pai nao foi alocado
	if(parent != NULL)
	{
		// Lista de arquivos do Diretorio pai nao foi alocada
		if(parent->files != NULL)
		{
			int i;
			struct t2fs_record_node* currentFile = parent->files->first;
			for(i = 0; i < parent->files->size; i++)
			{
				printRecord(currentFile);
				currentFile = currentFile->next;
			}
		}
	}
}

int initNodeRecordsFromDiskSector(t2fs_record_node* node, int firstSector, int lastSector)
{
	// faz a leitura dos setores pertencentes ao nodo
	unsigned int i; 
	for(i = firstSector; i <= lastSector; i++)
	{
		char* buffer = (char*) malloc(sizeof(char)*SECTOR_SIZE);
		if(buffer == NULL) return ERROR;
		read_sector(i, buffer);

		unsigned int currentRecord;
		// itera sobre os registros de um setor
		for(currentRecord = 0; currentRecord < MAX_RECORDS_IN_A_SECTOR; currentRecord++)
		{
			// printf("sector: %i - currentRecord: %i - total records: %i\n", i, currentRecord, MAX_RECORDS_IN_A_SECTOR);
			struct t2fs_record record;
			memcpy(&record, &(buffer[(sizeof(struct t2fs_record))*currentRecord]), sizeof(struct t2fs_record));
			// Cria o novo nodo e o adiciona como sub pasta ou arquivo, dependendo
			// do seu TypeVal
			t2fs_record_node* child = (t2fs_record_node*) malloc(sizeof(t2fs_record_node));
			if(child == NULL) return ERROR;
			child->record = record;
			child->parent = node;
			child->sub_folders = NULL;
			child->files = NULL;
			child->next = NULL;

			//to-do: colocar esse registro na lista do root
			switch(record.TypeVal)
			{
				case TYPEVAL_DIRETORIO:
					addSubDir(node,child);
					break;

				case TYPEVAL_REGULAR:
					addFile(node, child);
					break;

				case TYPEVAL_INVALIDO:
					break;
			}
	 	}
		
	}
	return SUCCESS;
}



int initNodeRecordsFromDiskCluster(t2fs_record_node* node, int firstCluster, t2fs_record_node* parent)
{

	// itera sobre os clusters ate que encontre um
	// cluster com valor invalido na FAT
	// (ou livre, o q significa que nao existem registros para leitura)
	int currentCluster = firstCluster;
	while(getCluster(currentCluster) != TYPEVAL_LIVRE &&
		getCluster(currentCluster) != TYPEVAL_BADSECTOR &&
		getCluster(currentCluster) != TYPEVAL_NAOPODEAPARECER)
	{

		int firstSector = getSectorFromCluster(currentCluster);
		int lastSector = firstSector + _superblock.SectorPerCluster;

		// faz a leitura dos setores pertencentes ao cluster
		unsigned int i;
		for(i = firstSector; i <= lastSector; i++)
		{
			char* buffer = (char*) malloc(sizeof(char)*SECTOR_SIZE);
			if(buffer == NULL) return ERROR;
			read_sector(i, buffer);

			unsigned int currentRecord;
			// itera sobre os registros de um setor
			for(currentRecord = 0; currentRecord < MAX_RECORDS_IN_A_SECTOR; currentRecord++)
			{
				// printf("sector: %i - currentRecord: %i - total records: %i\n", i, currentRecord, MAX_RECORDS_IN_A_SECTOR);
				struct t2fs_record record;
				memcpy(&record, &(buffer[(sizeof(struct t2fs_record))*currentRecord]), sizeof(struct t2fs_record));
				// Cria o novo nodo e o adiciona como sub pasta ou arquivo, dependendo
				// do seu TypeVal
				t2fs_record_node* child = (t2fs_record_node*) malloc(sizeof(t2fs_record_node));
				if(child == NULL) return ERROR;
				child->record = record;
				child->parent = parent;
				child->sub_folders = NULL;
				child->files = NULL;
				child->next = NULL;

				//to-do: colocar esse registro na lista do root
				switch(record.TypeVal)
				{
					case TYPEVAL_DIRETORIO:
						addSubDir(node, child);
						break;

					case TYPEVAL_REGULAR:
						addFile(node, child);
						break;

					case TYPEVAL_INVALIDO:
						break;
				}
		 	}
			
		}

		if(getCluster(currentCluster) == TYPEVAL_ULTIMOCLUSTER) return SUCCESS;
		else currentCluster = getNextCluster(currentCluster);
	}
	return ERROR;
}


int cd(char* folder)
{
	if(cwd == NULL) return ERROR;

	if(strcmp(folder, "..") == 0) 
		if(cdUp() == SUCCESS) return SUCCESS;
	
	if(strcmp(folder, ".") == 0) return SUCCESS;

	if(cwd->sub_folders == NULL) return ERROR;
	if(cwd->sub_folders->size == 0) return ERROR;
	
	int i;
	t2fs_record_node* currentNode = cwd->sub_folders->first;
	for(i = 0; i < cwd->sub_folders->size; i++)
	{
		if(strcmp(currentNode->record.name, folder) == 0) 
		{
			cwd = currentNode;
			return SUCCESS;
		}
		else currentNode = currentNode->next;
	}
	return ERROR;
}

int cdUp()
{
	// cwd == root
	if(cwd->parent == NULL) return ERROR;

	// cwd != root
	cwd = cwd->parent;
	return SUCCESS;
}