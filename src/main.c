#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/t2fs.h"

int main(int argc, char* argv[])
{
	char* developer = malloc(sizeof(char)*50);
	if(identify2(developer, 50) == 0) printf("developer: %s \n", developer);
	printf("\n");

	char* cwd = malloc(sizeof(char)*50);
	if(getcwd2(cwd, 50) == 0) printf("current working directory: %s \n", cwd);
	printf("\n");

	char* cd = (char*) malloc(sizeof(char)*50);
	strcpy(cd, ".\\subdir");

	if(chdir2(cd) == 0) printf("chdir %s ok! \n", cd);
	printf("\n");

	if(getcwd2(cwd, 50) == 0) printf("current working directory: %s \n", cwd);
	printf("\n");


	strcpy(cd, "..");
	DIR2 status1 = opendir2 (cd);
	if(status1 != -1) printf("opendir2 ok - DIR2: %i \n",status1);
	printf("\n");


	strcpy(cd, "..\\subdir");
	DIR2 status2 = opendir2 (cd);
	if(status2 != -1) printf("opendir2 ok - DIR2: %i \n",status2);
	printf("\n");


	DIRENT2 entry;
	if(readdir2(status1, &entry) == 0) printf("readdir2 ok \t entry: %s \n", entry.name);
	if(readdir2(status1, &entry) == 0) printf("readdir2 ok \t entry: %s \n", entry.name);
	if(readdir2(status1, &entry) == 0) printf("readdir2 ok \t entry: %s \n", entry.name);	
	if(readdir2(status1, &entry) == 0) printf("readdir2 ok \t entry: %s \n", entry.name);	
	printf("\n");


	free(developer);
	free(cwd);
	free(cd);
	return 0;
}
