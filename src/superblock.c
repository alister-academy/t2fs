#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/t2fs.h" /* estrutura do superbloco */
#include "../include/superblock.h" /* prototipos */
#include "../include/apidisk.h" /* metodos para leitura do setor 0 (dados do superbloco) */

#define SUPERBLOCK_SECTOR 0

#define SUCCESS 0
#define ERROR -1

/* Realiza a leitura do setor 0 do disco (superbloco)
*  e atribui os dados lidos a variavel superblock
*
*  Retorna 0 em caso de sucesso ou -1 caso contrario
*/
int initSuperblock()
{
	// printf("Criando objeto _superblock a partir do conteudo do disco...\n");
	if(read_sector(SUPERBLOCK_SECTOR, (char*) &_superblock) != SUCCESS)
	{
		// printf("Leitura de superbloco mal sucedida. \n");
		return ERROR;
	}
	// printf("Inicializacao bem sucedida!\n");
	numClusters = (_superblock.NofSectors - _superblock.DataSectorStart) / _superblock.SectorPerCluster;

	// printSuperblockData();
	// printf("Numero de Clusters: %u\n", numClusters);
	return SUCCESS;
}


/* Imprime o conteudo do superbloco lido no arquivo 
*  t2fs_disk.dat
*/
void printSuperblockData()
{
	// printf("\n\t--- Superblock Info ---\t\n");
	// printf(" *\t Id: %.4s \n",_superblock.Id);
	// printf(" *\t Version: %.4X \n", _superblock.Version);
	// printf(" *\t Size: %u \n", _superblock.SuperBlockSize);
	// printf(" *\t Disk size: %i \n", _superblock.DiskSize);
	// printf(" *\t No FSectors: %i \n", _superblock.NofSectors);
	// printf(" *\t Sector Per Cluster: %i \n", _superblock.SectorPerCluster);
	// printf(" *\t Primary Fat Sector: %i \n", _superblock.pFATSectorStart);
	// printf(" *\t Secondary FAT Sector: %i \n", _superblock.sFATSectorStart);
	// printf(" *\t Root Sector: %i \n", _superblock.RootSectorStart);
	// printf(" *\t Data Sector: %i \n", _superblock.DataSectorStart);
	// printf(" *\t No FDir Entries: %i \n", _superblock.NofDirEntries);
	// printf("\n");
}
