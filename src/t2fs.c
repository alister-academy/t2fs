#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "../include/t2fs.h" /* estruturas gerais e metodos 'alto nivel' para uso do disco */
#include "../include/superblock.h" /* metodos para controle do superbloco */
#include "../include/fat.h" /* metodos para controle da fat primaria e secundaria */
#include "../include/record.h" /* inicializacao e controle da arvore de registros */
#include "../include/apidisk.h" /* metodos para leitura e escrita em disco */

#define DEVELOPERS "Arthur Lenz 218316"

#define SUCCESS 0
#define ERROR -1

#define MAX_DIRS_OPEN 20
#define MAX_FILES_OPEN 20


char devs[] = DEVELOPERS;

bool disk_info_initialized = false;

char cwdPath[1048] = "/";

t2fs_record_node* openFiles[MAX_FILES_OPEN] = {0};
int openFilesCounter = 0;


typedef struct {
	t2fs_record_node* dir;
    int entry_counter;
    bool opened;
} OPEN_DIR;

OPEN_DIR openDirectories[MAX_DIRS_OPEN];

/*
***********************************************************
**********     Task 2 - File System - 2015/2     **********
***********************************************************
**********  Alunos:                 Cartão:      **********
**********                                       **********
**********    Arthur Lenz               218316   **********
***********************************************************
*/

/** Usada para identificar os desenvolvedores do T2FS **/
int identify2 (char *name, int size)
{
	if(size < sizeof(devs)) return ERROR;
	else
	{
		strncpy(name, devs, sizeof(devs));
		return SUCCESS;
	}
}


/** Criar um novo arquivo **/
FILE2 create2 (char *filename)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	// TO-DO
	// printf("Method not implemented!\n");
	return ERROR;
}


/** Apagar um arquivo do disco **/
int delete2 (char *filename)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	// TO-DO
	// printf("Method not implemented!\n");
	return ERROR;
}


/** Abre um arquivo existente no disco **/
FILE2 open2 (char *filename)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	// TO-DO
	// printf("Method not implemented!\n");
	return ERROR;
}


/** Fecha o arquivo identificado pelo parâmetro "handle" **/
int close2 (FILE2 handle)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	// TO-DO
	// printf("Method not implemented!\n");
	return ERROR;
}


/** Realiza a leitura de "size" bytes do arquivo identificado por "handle" **/
int read2 (FILE2 handle, char *buffer, int size)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	// TO-DO
	// printf("Method not implemented!\n");
	return ERROR;
}


/** Realiza a escrita de "size" bytes no arquivo identificado por "handle" **/
int write2 (FILE2 handle, char *buffer, int size)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	// TO-DO
	// printf("Method not implemented!\n");
	return ERROR;
}


/** Reposiciona o contador de posições (current pointer) do arquivo identificado por "handle" **/
int seek2 (FILE2 handle, unsigned int offset)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	// TO-DO
	// printf("Method not implemented!\n");
	return ERROR;
}


/** Criar um novo diretório **/
int mkdir2 (char *pathname)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	// TO-DO
	// printf("Method not implemented!\n");
	return ERROR;
}


/** Apagar um subdiretório do disco **/
int rmdir2 (char *pathname)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	// TO-DO
	// printf("Method not implemented!\n");
	return ERROR;
}


/** Abre um diretório existente no disco **/
DIR2 opendir2 (char *pathname)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	// itera sobre os 20 possiveis diretorios a serem abertos
	// se encontrar um espaco para alocar um novo diretorio aberto,
	// para de iterar
	// e seu indice fica na posicao openedDir, usada mais adiante
	// caso esteja cheio, retorna erro
	int openedDir;
	bool full = true;
	for(openedDir = 0; openedDir < MAX_DIRS_OPEN; openedDir++)
	{
		if(openDirectories[openedDir].opened == false)
		{
			full = false;
			break;
		}
	}
	
	if(full) return ERROR;

	if(pathname == NULL) return ERROR;
	
	char* localPathname = (char*) malloc(sizeof(pathname));
	strcpy(localPathname, pathname);

	char* dir = strtok(localPathname, "\\");
	if(dir == NULL) return ERROR;
	
	t2fs_record_node* originalCWD = cwd;

	// tenta entrar na pasta
	// 'pathname' usando o metodo cd implementado
	// na record
	// ele usa a variavel cwd global
	// entao devemos setar ela ao valor original no final desta funcao
	while(dir != NULL)
	{
		if(cd(dir) == ERROR)
		{
			cwd = originalCWD;
			return ERROR;
		}
		else dir = strtok(NULL, "\\");
	}

	t2fs_record_node* savedNode = cwd;

	openDirectories[openedDir].dir = savedNode;
	openDirectories[openedDir].entry_counter = 0;
	openDirectories[openedDir].opened = true;

	DIR2 ret = openedDir;

	// volta a apontar o cwd
	// para onde estava apontando
	// antes da chamada da funcao
	cwd = originalCWD;

	return ret;
}


/** Realiza a leitura das entradas do diretório identificado por "handle" **/
int readdir2 (DIR2 handle, DIRENT2 *dentry)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	if(handle < 0 || handle > 20) return ERROR;
	
	if(dentry == NULL) return ERROR;
	
	if(openDirectories[handle].dir == NULL) return ERROR;
	if(openDirectories[handle].dir->sub_folders == NULL) return ERROR;
	if(openDirectories[handle].dir->files == NULL) return ERROR;
	

	int subfolderEntries = openDirectories[handle].dir->sub_folders->size;
	int fileEntries = openDirectories[handle].dir->files->size;

	int totalEntries = subfolderEntries + fileEntries;

	// ja iterou por todos os registros
	if(openDirectories[handle].entry_counter > totalEntries) return ERROR;

	
	int entry_counter = openDirectories[handle].entry_counter;


	// itera dentro dos registros das subpastas
	if(entry_counter < subfolderEntries)
	{
		int i;
		t2fs_record_node* subfolder = openDirectories[handle].dir->sub_folders->first;
		for(i = 0; i < subfolderEntries; i++)
		{
			if(entry_counter == i)
			{
				strcpy(dentry->name, subfolder->record.name);
				dentry->fileType = 1;
				dentry->fileSize = 0;
			}
			else
			{
				subfolder = subfolder->next;
			}
		}
		
		openDirectories[handle].entry_counter++;
		return SUCCESS;
	}
	
	// itera dentro dos registros dos arquivos
	if(entry_counter >= subfolderEntries && entry_counter < totalEntries)
	{
		int i;
		t2fs_record_node* file = openDirectories[handle].dir->files->first;
		for(i = 0; i < fileEntries; i++)
		{
			if(entry_counter-subfolderEntries == i)
			{
				strcpy(dentry->name, file->record.name);
				dentry->fileType = 0;
				dentry->fileSize = file->record.bytesFileSize;
			}
			else
			{
				file = file->next;
			}
		}
		openDirectories[handle].entry_counter++;
		return SUCCESS;
	}

	return ERROR;	
}


/** Fecha o diretório identificado pelo parâmetro "handle" **/
int closedir2 (DIR2 handle)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	if(handle >= MAX_DIRS_OPEN) return ERROR;

	if(openDirectories[handle].opened == false) return ERROR;

	openDirectories[handle].opened = false;
	openDirectories[handle].dir = NULL;
	openDirectories[handle].entry_counter = 0;

	return SUCCESS;
}


/** Altera o diretório atual de trabalho (working directory) **/
int chdir2 (char *pathname)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	if(pathname == NULL) return ERROR;
	
	char* localPathname = (char*) malloc(sizeof(pathname));
	strcpy(localPathname, pathname);

	char* dir = strtok(localPathname, "\\");
	if(dir == NULL) return ERROR;
	
	t2fs_record_node* originalCWD = cwd;

	while(dir != NULL)
	{
		if(cd(dir) == ERROR)
		{
			cwd = originalCWD;
			return ERROR;
		}
		else dir = strtok(NULL, "\\");
	}

	return SUCCESS;
}


/** Informa o diretório atual de trabalho **/
int getcwd2 (char *pathname, int size)
{
	if(!disk_info_initialized) {
		if(initDiskInfo() == ERROR) return ERROR;
	}

	if(cwd == NULL) return ERROR;
	// agora, para a gravacao do nome na string
	// passada por parametro
	// caso seja o root, grava apenas '\'
	if(strcmp(cwd->record.name, "\\") == 0)
	{
		if(size < sizeof(cwd->record.name)) return ERROR;
		strcpy(pathname, "\\");
		return SUCCESS;
	}
	// caso contrario, itera sobre o diretorio atual
	// ate chegar ao pai, salvando o nome das pastas
	// e depois monta a string
	else
	{
		char temp[20][256];
		// cria um cwd auxiliar para iterar
		t2fs_record_node* auxiliaryCWD = cwd;
		int depth = 0;
		while(auxiliaryCWD->parent != NULL)
		{
			// salva nos temporarios o nome da pasta
			strcpy(temp[depth], auxiliaryCWD->record.name);
			// passa o iterador para o pai da pasta atual
			auxiliaryCWD = auxiliaryCWD->parent;
			depth++;
		}

		char* currentPath = (char*) malloc(sizeof(char)*1024);
		strcpy(currentPath, "\\");
		int j;
		for(j = depth-1; j >= 0; j--)
		{
			strcat(currentPath, temp[j]);
		}

		if(size < sizeof(currentPath)) return ERROR;
		strcpy(pathname, currentPath);
		return SUCCESS;
	}
}

int initDiskInfo()
{
	if(initSuperblock() == SUCCESS)
	{
		if(initPrimaryFAT(numClusters) == SUCCESS &&
		initSecondaryFAT(numClusters, _superblock.sFATSectorStart) == SUCCESS)
		{
			if(initRecordTree() == SUCCESS)
			{
				disk_info_initialized = true;
				return SUCCESS;
			}
			else return ERROR;
		}
		else return ERROR;
	}
	else return ERROR;
}